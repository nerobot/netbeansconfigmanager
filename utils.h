#ifndef UTILS_H
#define UTILS_H

#include <QtWidgets>

namespace NetbeansConfigManager {

class Utils
{
public:
    Utils();

    static QString extractParam(const QString &name, const QString &data)
    {
        QString n = name+"=";
        int i = data.indexOf(n);
        if (i < 0) return "";
        i += n.length();
        int i2 = data.indexOf("\r", i);
        if (i2 < 0) i2 = data.length();
        return data.mid(i,i2-i);
    }

    static QStringList split(const QString &data, const QString &splitter)
    {
        QStringList list;
        int i = data.indexOf(splitter);
        if (i < 0) {
            list << data;
            return list;
        }
        list << data.mid(0, i);
        list << data.mid(i+splitter.length());
        return list;
    }

    static QString cellText(const QTableWidget *table, int row, int column)
    {
        QTableWidgetItem *i = table->item(row, column);
        return i ? i->text() : "";
    }

};

}
#endif // UTILS_H
