#ifndef SOURCEFILTERING_H
#define SOURCEFILTERING_H

#include <QtCore>

namespace NetbeansConfigManager {

class FilterItem;

class SourceFiltering
{
public:
    SourceFiltering();
    ~SourceFiltering();

    FilterItem *set(QString path, Qt::CheckState state, bool isFolder);
    QString exportExcludes();
    bool isEmpty();
    Qt::CheckState state(const QString &path, Qt::CheckState defValue = Qt::Checked);
    void clone(SourceFiltering *filter);
    FilterItem *find(const QString &path);
    FilterItem *add(QString path, Qt::CheckState state, bool isFolder);
private:
    QList<FilterItem*> *items;
    bool isEdited;

    void removeAll();
};



class FilterItem
{
public:
    FilterItem();
    FilterItem(QString path, Qt::CheckState state, bool isFolder);
    QString path;
    Qt::CheckState state;
    bool isFolder;
    int usages;
};


}

#endif // SOURCEFILTERING_H
