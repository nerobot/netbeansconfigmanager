#include "config.h"

using namespace NetbeansConfigManager;

Config::Config(QString name)
{
    this->name = name;
    prefix = "configs."+name+".";
    abilities = new NameValueSet();
    manifest = new NameValueSet();
    params = new NameValueSet();
    isDefault = false;
    filter = new SourceFiltering();
}

Config::~Config()
{
    removeAllManifest();
    removeAllAbilities();
    removeAllParams();
    delete params;
    delete abilities;
    delete manifest;
    delete filter;
}

void Config::readParams(QStringList &list, bool checkPrefix)
{
    QStringList listForRemove;
    int prefLen = prefix.length();
    foreach (QString s, list) {
        QString sss = s.trimmed();
        if (sss == "" || sss.startsWith("all.")) {
            continue;
        }
        if (checkPrefix) {
            if (sss.startsWith(prefix)) {
                sss = sss.mid(prefLen);
            } else {
                continue;
            }
        }
        QStringList l = Utils::split(sss, "=");
        params->add(l.at(0), l.at(1));
        listForRemove.append(s);
    }
    //
    foreach (QString s, listForRemove) {
        list.removeOne(s);
    }
    // abilities
    NameValue *nv;
    QStringList listSplit;
    nv = findParam(PARAM_ABILITIES);
    if (nv != 0) {
        if (!nv->value.isEmpty()) {
            listSplit = nv->value.split(",");
            foreach (QString nameValue, listSplit) {
                QStringList list2 = nameValue.split("=");
                abilities->add(list2.at(0), list2.at(1));
            }
        }
    } else {
        params->add(PARAM_ABILITIES, "");
    }
    // manifest
    nv = findParam(PARAM_MANIFEST);
    if (nv != 0) {
        if (!nv->value.isEmpty()) {
            listSplit = nv->value.split("\\n", QString::SkipEmptyParts);
            foreach (QString nameValue, listSplit) {
                QStringList list2 = nameValue.split(": ");
                manifest->add(list2.at(0), list2.at(1));
            }
        }
    } else {
        params->add(PARAM_MANIFEST, "");
    }
    // filtering
    nv = findParam(PARAM_FILTER);
    if (nv != 0) {
        if (!nv->value.isEmpty()) {
            //qDebug()<<"filters for "+caption;
            listSplit = nv->value.split(",");
            foreach (QString path, listSplit) {
                bool folder = false;
                if (path.endsWith("/**")) {// /** means need to exclude all of subitems
                    path = path.left(path.length()-3);
                    folder = true;
                }
                filter->add(path, Qt::Unchecked, folder);
            }
        }
    } else {
        params->add(PARAM_FILTER, "");
    }


}

void Config::cloneManifest(Config *conf)
{
    manifest->clone(conf->manifest);
}

void Config::cloneAbilities(Config *conf)
{
    abilities->clone(conf->abilities);
}

void Config::cloneParams(Config *conf)
{
    params->clone(conf->params);
}

void Config::cloneFilter(Config *conf)
{
    filter->clone(conf->filter);
}

void Config::addManifestParam(QString name, QString value)
{
    manifest->change(name, name, value);
}

void Config::addAbilityParam(QString name, QString value)
{
    abilities->change(name, name, value);
}

void Config::changeManifestParam(QString prevName, QString newName, QString value)
{
    manifest->change(prevName, newName, value);
}

bool Config::hasManifestParam(const QString &name)
{
    return manifest->has(name);
}

bool Config::hasAbilityParam(const QString &name)
{
    return abilities->has(name);
}

bool Config::hasParamsParam(const QString &name)
{
    return params->has(name);
}

void Config::changeAbilityParam(QString prevName, QString newName, QString value)
{
    abilities->change(prevName, newName, value);
}

void Config::changeParamsParam(QString prevName, QString newName, QString value)
{
    params->change(prevName, newName, value);
}

void Config::removeFromManifest(const QString &name)
{
    manifest->remove(name);
}

void Config::removeFromAbilities(const QString &name)
{
    abilities->remove(name);
}

void Config::removeFromParams(const QString &name)
{
    params->remove(name);
    //
    if (name == PARAM_ABILITIES) {
        removeAllAbilities();
    }
    if (name == PARAM_MANIFEST) {
        removeAllManifest();
    }
}

void Config::removeAllAbilities()
{
    abilities->removeAll();
}

void Config::removeAllManifest()
{
    manifest->removeAll();
}

void Config::removeAllParams()
{
    params->removeAll();
}


QString Config::paramName(const QString &name)
{
    return prefix+name;
}

NameValue* Config::findParam(const QString &paramName)
{
    return params->find(paramName);
}


void Config::clone(Config *conf)
{
    params->clone(conf->params);
    manifest->clone(conf->manifest);
    abilities->clone(conf->abilities);
    filter->clone(conf->filter);
}

QString Config::toString()
{
    QString s = "";
    foreach (NameValue *nv, *params->items) {
        QString n = nv->name;
        QString value;
        if (n == PARAM_MANIFEST) {
            value = getManifestValueLine();
        } else if (n == PARAM_ABILITIES) {
            value = getAbilityValueLine();
        } else if (n == PARAM_FILTER) {
            value = filter->exportExcludes();
        } else {
            value = nv->value;
        }
        if (s.length() > 0) {
            s += "\r\n";
        }
        if (!isDefault) {
            s += "configs." + name +".";
        }
        s += n + "=" + value;
    }
    return s;
}

QString Config::getManifestValueLine()
{
    QString s = "";
    foreach (NameValue *nv, *manifest->items) {
        s += nv->name + ": " + nv->value + "\\n";
    }
    return s;
}

QString Config::getAbilityValueLine()
{
    QString s = "";
    foreach (NameValue *nv, *abilities->items) {
        if (s.length() > 0) {
            s += ",";
        }
        s += nv->name + "=" + nv->value;
    }
    return s;
}
