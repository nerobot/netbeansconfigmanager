#include "sourcefiltering.h"

using namespace NetbeansConfigManager;

SourceFiltering::SourceFiltering()
{
    items = new QList<FilterItem*>();
    isEdited = false;
}

SourceFiltering::~SourceFiltering()
{
    removeAll();
    delete items;
}

void SourceFiltering::removeAll()
{
    foreach (FilterItem *i, *items) {
        items->removeOne(i);
    }
    items->clear();
}

FilterItem* SourceFiltering::set(QString path, Qt::CheckState state, bool isFolder)
{
    FilterItem *i = find(path);
    if (i == 0) {
        i = new FilterItem();
        items->append(i);
        ++i->usages;
    }
    i->path = path;
    i->state = state;
    i->isFolder = isFolder;
    isEdited = true;
    //qDebug()<<"filter"<<path<<"state"<<state<<"usages"<<i->usages;
}

FilterItem* SourceFiltering::add(QString path, Qt::CheckState state, bool isFolder)
{
    FilterItem *i = find(path);
    if (i != 0) {
        items->removeOne(i);
    }
    i = new FilterItem();
    items->append(i);
    i->path = path;
    i->state = state;
    i->isFolder = isFolder;
    i->usages = 0;
}

FilterItem* SourceFiltering::find(const QString &path)
{
    foreach (FilterItem *i, *items) {
        if (i->path == path) {
            ++i->usages;
            return i;
        }
    }
    return 0;
}

Qt::CheckState SourceFiltering::state(const QString &path, Qt::CheckState defValue)
{
    FilterItem *i = find(path);
    return i ? i->state : defValue;
}

QString SourceFiltering::exportExcludes()
{
    QList<FilterItem*> listAll, listFolders;
    foreach (FilterItem *i, *items) {
        if (i->state == Qt::Unchecked && (!isEdited || i->usages > 0)) {
            listAll.append(i);
            if (i->isFolder) {
                listFolders.append(i);
            }
        }
    }
    // remove subitems for totally unchecked folders
    foreach (FilterItem *ifold, listFolders) {
        foreach (FilterItem *i, listAll) {
            if (i->path.startsWith(ifold->path+"/")) {
                listAll.removeOne(i);
            }
        }
    }
    // generating output string
    QString s = "";
    foreach (FilterItem *i, listAll) {
        if (s.length() > 0) {
            s += ",";
        }
        s += i->path;
        if (i->isFolder) {
            s += ","+i->path+"/**";
        }
    }
    return s;
}

bool SourceFiltering::isEmpty()
{
    return items->isEmpty();
}

void SourceFiltering::clone(SourceFiltering *filter)
{
    removeAll();
    foreach (FilterItem *i, *filter->items) {
        FilterItem *fi = set(i->path, i->state, i->isFolder);
        fi->usages = i->usages;
    }
}


FilterItem::FilterItem()
{
    usages = 0;
}

FilterItem::FilterItem(QString path, Qt::CheckState state, bool isFolder)
{
    FilterItem();
    this->path = path;
    this->state = state;
    this->isFolder = isFolder;
}
