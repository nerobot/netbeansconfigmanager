#ifndef NAMEVALUESET_H
#define NAMEVALUESET_H

#include <QtCore>

namespace NetbeansConfigManager {

class NameValue;


class NameValueSet
{
public:
    NameValueSet();
    ~NameValueSet();

    QList<NameValue *> *items;

    void add(QString name, QString value);
    void change(QString prevName, QString newName, QString value);
    void remove(const QString &name);
    bool has(const QString &name);
    void removeAll();
    void clone(NameValueSet *set);
    NameValue *find(const QString &name);

    QString toString(const QString &splitter);

};


class NameValue {
public:
    NameValue(QString name, QString value) : name(name), value(value) {}
    QString name, value;
};

}
#endif // NAMEVALUESET_H
