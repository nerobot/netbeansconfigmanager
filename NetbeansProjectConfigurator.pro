#-------------------------------------------------
#
# Project created by QtCreator 2015-06-25T14:20:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NetbeansProjectConfigurator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    manager.cpp \
    config.cpp \
    utils.cpp \
    namevalueset.cpp \
    sourcefiltering.cpp

HEADERS  += mainwindow.h \
    manager.h \
    config.h \
    utils.h \
    namevalueset.h \
    sourcefiltering.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
