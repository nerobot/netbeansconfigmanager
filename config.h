#ifndef CONFIG_H
#define CONFIG_H

#include <QtCore>
#include <QMessageBox>
#include <utils.h>
#include <namevalueset.h>
#include <sourcefiltering.h>


namespace NetbeansConfigManager {

static const QString PARAM_ABILITIES = "abilities";
static const QString PARAM_MANIFEST = "manifest.others";
static const QString PARAM_FILTER = "filter.excludes";
static const QString PARAM_ALL_CONFIGS = "all.configurations";


class Config
{
public:
    Config(QString name);
    ~Config();

    QString name;
    SourceFiltering *filter;
    bool isDefault;
    NameValueSet *abilities, *manifest, *params;

    void readParams(QStringList &list, bool checkPrefix=true);
    void cloneManifest(Config *conf);
    void cloneAbilities(Config *conf);
    void addManifestParam(QString name, QString value);
    void addAbilityParam(QString name, QString value);
    void changeManifestParam(QString prevName, QString newName, QString value);
    void changeAbilityParam(QString prevName, QString newName, QString value);
    void changeParamsParam(QString prevName, QString newName, QString value);
    void removeFromManifest(const QString &name);
    void removeFromAbilities(const QString &name);
    void removeFromParams(const QString &name);

    bool hasManifestParam(const QString &name);
    bool hasAbilityParam(const QString &name);
    bool hasParamsParam(const QString &name);

    void clone(Config *conf);
    QString toString();
    void cloneFilter(Config *conf);
    void cloneParams(Config *conf);

private:
    QString prefix;

    QString paramName(const QString &name);
    NameValue *findParam(const QString &paramName);
    void removeAllAbilities();
    void removeAllManifest();
    void removeAllParams();
    QString getManifestValueLine();
    QString getAbilityValueLine();
};


}
#endif // CONFIG_H
