#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QTableWidget>
#include <manager.h>

using namespace NetbeansConfigManager;

static QSettings* settings()
{
    static QSettings *settings = 0;
    if (settings == 0) {
        settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, "FingerDev Studio", "Netbeans Project Configurator");
    }
    return settings;
}

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void signalLoadProperties(QString path);


protected:
    bool eventFilter(QObject *obj, QEvent *event);

private slots:

    void onLoadProperties(QString path);

    void on_listWidgetConfigs_clicked(const QModelIndex &index);

    void on_toolButtonManifestAdd_clicked();

    void on_toolButtonAbilitiesAdd_clicked();

    void on_toolButtonManifestRemove_clicked();

    void on_toolButtonAbilitiesRemove_clicked();

    void on_toolButtonConfigRemove_clicked();

    void on_listWidgetConfigs_doubleClicked(const QModelIndex &index);

    void on_toolButtonManifestCloneFromPinned_clicked();

    void on_toolButtonAbilitiesCloneFromPinned_clicked();

    void on_toolButtonManifestDeployToSelConfigs_clicked();

    void on_toolButtonAbilitiesDeployToSelConfigs_clicked();

    void on_toolButtonExportChoosePath_clicked();

    void on_toolButtonImportChoosePath_clicked();

    void on_toolButtonParamsRemove_clicked();

    void on_toolButtonParamsAdd_clicked();

    void on_tableWidgetManifest_cellChanged(int row, int column);

    void on_tableWidgetManifest_cellClicked(int row, int column);

    void on_tableWidgetAbilities_cellClicked(int row, int column);

    void on_tableWidgetAbilities_cellChanged(int row, int column);

    void on_tableWidgetParams_cellClicked(int row, int column);

    void on_tableWidgetParams_cellChanged(int row, int column);

    void on_toolButtonConfigAdd_clicked();

    void on_toolButtonConfigRename_clicked();

    void on_toolButtonConfigClone_clicked();

    void on_pushButtonExport_clicked();

    void on_toolButtonChooseFilteringRootPath_clicked();

    void on_treeSourceFiltering_itemClicked(QTreeWidgetItem *item, int column);

    void on_toolButtonFilteringCloneFromPinned_clicked();

    void on_treeSourceFiltering_customContextMenuRequested(const QPoint &pos);

    void on_toolButtonParamsCloneFromPinned_clicked();

    void on_toolButtonReloadConfig_clicked();

    void on_tableWidgetManifest_customContextMenuRequested(const QPoint &pos);

private:
    Ui::MainWindow *ui;
    Manager manager;
    QString lastImportPath, lastExportPath, lastFilteringRootPath;
    Config *pinnedConfig, *activeConfig;
    QString manifestStoredName, abilityStoredName, paramsStoredName;
    bool isListenToTableChanges;
    QMenu *filterPopup, *manifestPopup;

    void chooseConfig(int row);
    void loadProperties(const QString &path);
    void onPinnedConfigChanged();
    void fillManifestUi();
    void fillAbilitiesUi();
    void fillParamsUi();
    void setExportPath(QString path);
    void setFilteringRootPath(QString path);
    void addItemsToTree(QTreeWidget *tree, QTreeWidgetItem *item, const QFileInfoList &list);
    void setItemState(QTreeWidgetItem *item, Qt::CheckState state, bool updateParentItemState=true, bool updateChildrenItemsState=false);
    QString itemPath(QTreeWidgetItem *item);
    void scanItemsInTree(QTreeWidgetItem *parentItem, int type);
    void onScanItemsInTreeCallback(QTreeWidgetItem *item, int type);
    void adjustConfigFilter();
};

#endif // MAINWINDOW_H
