#include "manager.h"

using namespace NetbeansConfigManager;


Manager::Manager()
{
    activeConf = 0;
}

Manager::~Manager()
{
    removeAll();
}

void Manager::loadProperties(const QString &path)
{
    removeAll();
    QFile file(path);
    if (!file.exists()) {
        //QMessageBox::information(0 ,"Load Properties", "File not found!\n"+path);
        return;
    }
    file.open(QFile::ReadOnly);
    dataText = QString(file.readAll());
    file.close();
    dataList = dataText.split("\r\n", QString::SkipEmptyParts);
    parseConfigs();
}

QList<Config *> Manager::getConfigs()
{
    return configs;
}

void Manager::parseConfigs() {

    QString param;
    param = extractParam(PARAM_ALL_CONFIGS);
    QStringList confNames = param.split(",");
    confNames.removeAt(0); // remove '\ ' - default config
    foreach (QString name, confNames) {
        Config *conf = new Config(name);
        conf->readParams(dataList);
        configs.append(conf);
    }
    // remove broken configs - which not exist in config list
    foreach (QString s, dataList) {
        if (s.startsWith("configs.")) {
            dataList.removeOne(s);
        }
    }
    //
    Config *conf = new Config("!Default");
    conf->readParams(dataList, false);
    conf->isDefault = true;
    configs.append(conf);
}

QString Manager::extractParam(const QString &paramName)
{
    return Utils::extractParam(paramName, dataText);
}

Config *Manager::find(const QString &configName)
{
    foreach (Config *c, configs) {
        if (c->name == configName) {
            return c;
        }
    }
    return 0;
}

bool Manager::has(const QString &configName)
{
    foreach (Config *c, configs) {
        if (c->name == configName) {
            return true;
        }
    }
    return false;
}

void Manager::activate(const QString &configName)
{
    Config *c = find(configName);
    if (c != 0) {
        activeConf = c;
    }
}

Config *Manager::activeConfig()
{
    return activeConf;
}

void Manager::add(QString configName)
{
    Config *c = new Config(configName);
    configs.append(c);
}

void Manager::remove(const QString &configName)
{
    foreach (Config *c, configs) {
        if (c->name == configName) {
            configs.removeOne(c);
            delete c;
            break;
        }
    }
}
void Manager::removeAll()
{
    foreach (Config *c, configs) {
        delete c;
    }
    configs.clear();
}

void Manager::rename(const QString &prevConfigName, QString newConfigName)
{
    Config *c = find(prevConfigName);
    if (c) {
        c->name = newConfigName;
    }
}

void Manager::clone(const QString &prevConfigName, QString newConfigName)
{
    Config *c = find(prevConfigName);
    if (c) {
        Config *newConf = new Config(newConfigName);
        newConf->clone(c);
        configs.append(newConf);
    }
}

QString Manager::exportAll()
{
    QString s = "";
    QString allConf = "\\ ";
    foreach (Config *c, configs) {
        if (s.length() > 0) {
            s += "\r\n";
        }
        s += c->toString();
        if (!c->isDefault) {
            allConf += "," + c->name;
        }
    }
    s = PARAM_ALL_CONFIGS + "=" + allConf + "\r\n" + s;
    return s;
}
