#include "namevalueset.h"

using namespace NetbeansConfigManager;


NameValueSet::NameValueSet()
{
    items = new QList<NameValue *>();
}

NameValueSet::~NameValueSet()
{
    removeAll();
    delete items;
}

void NameValueSet::add(QString name, QString value)
{
    if (name == "") return;
    items->append(new NameValue(name, value));
}

void NameValueSet::change(QString prevName, QString newName, QString value)
{
    if (newName == "") return;
    foreach (NameValue *nv, *items) {
        if (nv->name == prevName) {
            nv->name = newName;
            nv->value = value;
            return;
        }
    }
    add(newName, value);
}

void NameValueSet::remove(const QString &name)
{
    foreach (NameValue *nv, *items) {
        if (nv->name == name) {
            items->removeOne(nv);
        }
    }
}

void NameValueSet::removeAll()
{
    foreach (NameValue *nv, *items) {
        delete nv;
    }
    items->clear();
}

void NameValueSet::clone(NameValueSet *set)
{
    removeAll();
    foreach (NameValue *nv, *set->items) {
        add(nv->name, nv->value);
    }
}

NameValue *NameValueSet::find(const QString &name)
{
    foreach (NameValue *nv, *items) {
        if (nv->name == name) {
            return nv;
        }
    }
    return 0;
}

bool NameValueSet::has(const QString &name)
{
    foreach (NameValue *nv, *items) {
        if (nv->name == name) {
            return true;
        }
    }
    return false;
}

QString NameValueSet::toString(const QString &splitter)
{
    QString s = "";
    foreach (NameValue *nv, *items) {
        if (s.length() > 0) {
            s += splitter;
        }
        s += nv->name + "=" + nv->value;
    }
    return s;
}
