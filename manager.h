#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H
#include <QFile>
#include <QMessageBox>

#include <config.h>
#include <utils.h>

namespace NetbeansConfigManager {

static const QString DEFAULT_CONFIG_NAME = "\\ ";

class Manager
{
public:
    Manager();
    ~Manager();
    void loadProperties(const QString &path);
    QList<Config *> getConfigs();

    void activate(const QString &configName);
    Config *activeConfig();

    Config *find(const QString &configName);
    bool has(const QString &configName);
    void add(QString configName);
    void rename(const QString &prevConfigName, QString newConfigName);
    void clone(const QString &prevConfigName, QString newConfigName);
    QString exportAll();
    void remove(const QString &configName);
private:
    QString dataText;
    QStringList dataList;
    QList<Config *> configs;
    Config *activeConf;

    void parseConfigs();
    QString extractParam(const QString &paramName);

    void removeAll();
};

}
#endif // CONFIGMANAGER_H
