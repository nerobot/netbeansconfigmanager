#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <qfiledialog.h>

const QString EXPORT_PATH = "lastExportPath";
const QString IMPORT_PATH = "lastImportPath";
const QString FILTERING_PATH = "lastFilteringRootPath";
const QString WINDOW_GEOMETRY = "windowGeomenty";


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QCoreApplication::setOrganizationName("FingerDev Studio");
    QCoreApplication::setOrganizationDomain("fingerdev.com");
    QCoreApplication::setApplicationName("Netbeans Project Configurator");

    ui->setupUi(this);

    activeConfig = 0;
    pinnedConfig = 0;
    onPinnedConfigChanged();

    isListenToTableChanges = false;

    ui->tableWidgetManifest->setHorizontalHeaderLabels(QStringList()<<"Name"<<"Value");
    ui->tableWidgetAbilities->setHorizontalHeaderLabels(QStringList()<<"Name"<<"Value");
    ui->tableWidgetParams->setHorizontalHeaderLabels(QStringList()<<"Name"<<"Value");
    ui->tableWidgetManifest->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidgetAbilities->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidgetParams->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    lastImportPath = settings()->value(IMPORT_PATH,"").toString();
    if (lastImportPath != "") {
        loadProperties(lastImportPath);
    }

    lastExportPath = settings()->value(EXPORT_PATH,"").toString();
    if (lastExportPath == "") {
        setExportPath(lastImportPath);
    } else {
        ui->labelExportPath->setText(lastExportPath);
    }

    setFilteringRootPath( settings()->value(FILTERING_PATH,"").toString() );

    QByteArray val = settings()->value(WINDOW_GEOMETRY).toByteArray();
    if (!val.isEmpty()) {
        this->restoreGeometry(val);
    }

    filterPopup = new QMenu(this);
    filterPopup->addAction("Uncheck");
    filterPopup->addAction("Check");
    ui->treeSourceFiltering->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->treeSourceFiltering->installEventFilter(this);

    manifestPopup = new QMenu(this);
    manifestPopup->addAction("Apply to selected configs");
    ui->tableWidgetManifest->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidgetManifest->installEventFilter(this);

    connect(this, SIGNAL(signalLoadProperties(QString)), this, SLOT(onLoadProperties(QString)));
}

MainWindow::~MainWindow()
{
    settings()->setValue(WINDOW_GEOMETRY, this->saveGeometry());
    delete settings();
    delete ui;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    // change item state by pressing +/-
    // useful for folders, because they are locked for mouse click on ckeckbox
    QTreeWidget *tree = ui->treeSourceFiltering;
    if (obj == tree) {
        if (event->type() == QEvent::KeyPress) {
            QList<QTreeWidgetItem *> selItems = tree->selectedItems();
            if (selItems.isEmpty()) return true;
            QTreeWidgetItem *i = selItems.at(0);
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if (keyEvent->key() == Qt::Key_Plus) {
                setItemState(i, Qt::Checked, true, true);
            } else if (keyEvent->key() == Qt::Key_Minus) {
                setItemState(i, Qt::Unchecked, true, true);
            }
            return true;
        }
    }
    return QMainWindow::eventFilter(obj, event);
}

void MainWindow::onLoadProperties(QString path)
{
    settings()->setValue(IMPORT_PATH, path);
    loadProperties(path);
    //
    if (lastExportPath == "") {
        setExportPath(path);
    }
}

void MainWindow::setExportPath(QString path)
{
    lastExportPath = path;
    settings()->setValue(EXPORT_PATH, lastExportPath);
    ui->labelExportPath->setText(lastExportPath);
}

void MainWindow::loadProperties(const QString &path)
{
    manager.loadProperties(path);
    activeConfig = pinnedConfig = 0;

    ui->labelImportPath->setText(path);
    ui->listWidgetConfigs->clear();
    QList<Config *> list = manager.getConfigs();
    foreach (Config *conf, list) {
        ui->listWidgetConfigs->addItem(conf->name);
    }
    if (!list.isEmpty()) {
        chooseConfig(0);
    }
}

void MainWindow::on_listWidgetConfigs_clicked(const QModelIndex &index)
{
    chooseConfig(index.row());
}

void MainWindow::chooseConfig(int row)
{
    QListWidgetItem *item = ui->listWidgetConfigs->item(row);
    QString configName = item->text();
    if (activeConfig != 0 && activeConfig->name == configName) return; //the same config
    //save changes for current

    //
    manager.activate(configName);
    activeConfig = manager.activeConfig();
    fillManifestUi();
    fillAbilitiesUi();
    fillParamsUi();
    ui->labelActiveConfig->setText("active: "+activeConfig->name);
    ui->listWidgetConfigs->setCurrentRow(row);
    adjustConfigFilter();
}

void MainWindow::fillManifestUi()
{
    QList<NameValue *> *list = activeConfig->manifest->items;
    ui->tableWidgetManifest->setRowCount(list->size());
    ui->tableWidgetManifest->clearContents();
    QTableWidgetItem *i;
    int tableRow = 0;
    isListenToTableChanges = false;
    foreach (NameValue *nv, *list) {
        i = new QTableWidgetItem(nv->name);
        ui->tableWidgetManifest->setItem(tableRow, 0, i);
        i = new QTableWidgetItem(nv->value);
        ui->tableWidgetManifest->setItem(tableRow, 1, i);
        ++tableRow;
    }
    isListenToTableChanges = true;
}

void MainWindow::fillAbilitiesUi()
{
    isListenToTableChanges = false;
    QList<NameValue *> *list = activeConfig->abilities->items;
    ui->tableWidgetAbilities->setRowCount(list->size());
    ui->tableWidgetAbilities->clearContents();
    QTableWidgetItem *i;
    int tableRow = 0;
    foreach (NameValue *nv, *list) {
        i = new QTableWidgetItem(nv->name);
        ui->tableWidgetAbilities->setItem(tableRow, 0, i);
        i = new QTableWidgetItem(nv->value);
        ui->tableWidgetAbilities->setItem(tableRow, 1, i);
        ++tableRow;
    }
    isListenToTableChanges = true;
}

void MainWindow::fillParamsUi()
{
    isListenToTableChanges = false;
    QList<NameValue *> *list = activeConfig->params->items;
    ui->tableWidgetParams->setRowCount(list->size());
    ui->tableWidgetParams->clearContents();
    QTableWidgetItem *i;
    int tableRow = 0;
    foreach (NameValue *nv, *list) {
        i = new QTableWidgetItem(nv->name);
        i->setFlags(i->flags() ^ Qt::ItemIsEditable);
        ui->tableWidgetParams->setItem(tableRow, 0, i);
        i = new QTableWidgetItem(nv->value);
        ui->tableWidgetParams->setItem(tableRow, 1, i);
        ++tableRow;
    }
    isListenToTableChanges = true;
}

void MainWindow::on_toolButtonManifestAdd_clicked()
{
    isListenToTableChanges = false;
    QTableWidget *table = ui->tableWidgetManifest;
    int count = table->rowCount();
    table->setRowCount(count+1);
    table->setCurrentCell(count, 0);
    isListenToTableChanges = true;
}

void MainWindow::on_toolButtonAbilitiesAdd_clicked()
{
    isListenToTableChanges = false;
    QTableWidget *table = ui->tableWidgetAbilities;
    int count = table->rowCount();
    table->setRowCount(count+1);
    table->setCurrentCell(count, 0);
    isListenToTableChanges = true;
}

void MainWindow::on_toolButtonManifestRemove_clicked()
{
    QTableWidget *table = ui->tableWidgetManifest;
    int r = table->currentRow();
    if (r < 0) return;
    //
    QString name = Utils::cellText(table, r, 0);
    if (name != "") {
        activeConfig->removeFromManifest(name);
    }
    //
    table->removeRow(r);
    int rows = table->rowCount();
    if (r >= rows) {
        r = rows-1;
    }
    table->selectRow(r);
}


void MainWindow::on_toolButtonAbilitiesRemove_clicked()
{
    QTableWidget *table = ui->tableWidgetAbilities;
    int r = table->currentRow();
    if (r < 0) return;
    //
    QString name = Utils::cellText(table, r, 0);
    if (name != "") {
        activeConfig->removeFromAbilities(name);
    }
    //
    table->removeRow(r);
    int rows = table->rowCount();
    if (r >= rows) {
        r = rows-1;
    }
    table->selectRow(r);
}

void MainWindow::on_toolButtonConfigRemove_clicked()
{
    int r = ui->listWidgetConfigs->currentRow();
    if (r < 0) return;
    QList<QListWidgetItem *> items = ui->listWidgetConfigs->selectedItems();
    QMessageBox mbox;
    QString message;
    if (items.size() == 1) {
        message = "Removing config "+items.at(0)->text()+".";
    } else {
        message = "Removing configs ";
        int index = 0;
        foreach (QListWidgetItem *i, items) {
            if (index > 0) {
                message += ", ";
            }
            message += i->text();
            ++index;
        }
        message += ".";
    }
    mbox.setText(message);
    mbox.setInformativeText("Really?");
    mbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    mbox.setDefaultButton(QMessageBox::No);
    int ret = mbox.exec();
    if (ret == QMessageBox::Yes) {
        foreach (QListWidgetItem *i, items) {
            ui->listWidgetConfigs->removeItemWidget(i);
            manager.remove(i->text());
            delete i;
        }
        int rows = ui->listWidgetConfigs->count();
        if (r >= rows) {
            r = rows-1;
        }
        ui->listWidgetConfigs->setCurrentRow(r);
    }
}

void MainWindow::on_listWidgetConfigs_doubleClicked(const QModelIndex & /*index*/)
{
    if (pinnedConfig == 0 || pinnedConfig->name != activeConfig->name) {
        pinnedConfig = activeConfig;
        ui->labelPinnedConfig->setText(pinnedConfig->name);
    } else {
        pinnedConfig = 0;
        ui->labelPinnedConfig->setText("<none>");
    }
    onPinnedConfigChanged();
}

void MainWindow::onPinnedConfigChanged()
{
    bool enabled = (pinnedConfig != 0);
    ui->toolButtonManifestCloneFromPinned->setEnabled(enabled);
    ui->toolButtonAbilitiesCloneFromPinned->setEnabled(enabled);
}

void MainWindow::on_toolButtonManifestCloneFromPinned_clicked()
{
    if (activeConfig == 0 || pinnedConfig == 0) return;
    if (activeConfig == pinnedConfig) return;
    activeConfig->cloneManifest(pinnedConfig);
    fillManifestUi();
}

void MainWindow::on_toolButtonAbilitiesCloneFromPinned_clicked()
{
    if (activeConfig == 0 || pinnedConfig == 0) return;
    if (activeConfig == pinnedConfig) return;
    activeConfig->cloneAbilities(pinnedConfig);
    fillAbilitiesUi();
}

void MainWindow::on_toolButtonManifestDeployToSelConfigs_clicked()
{
    QTableWidget *table = ui->tableWidgetManifest;
    int r = table->currentRow();
    if (r < 0) return;
    QList<QListWidgetItem *> items = ui->listWidgetConfigs->selectedItems();
    if (items.size() < 2) return;
    //
    QString name = table->item(r, 0)->text();
    QString value = table->item(r, 1)->text();
    foreach (QListWidgetItem *i, items) {
        Config *c = manager.find(i->text());
        if (c != 0 && c != activeConfig) {
            c->addManifestParam(name, value);
        }
    }
}

void MainWindow::on_toolButtonAbilitiesDeployToSelConfigs_clicked()
{
    QTableWidget *table = ui->tableWidgetAbilities;
    int r = table->currentRow();
    if (r < 0) return;
    QList<QListWidgetItem *> items = ui->listWidgetConfigs->selectedItems();
    if (items.size() < 2) return;
    //
    QString name = table->item(r, 0)->text();
    QString value = table->item(r, 1)->text();
    foreach (QListWidgetItem *i, items) {
        Config *c = manager.find(i->text());
        if (c != 0 && c != activeConfig) {
            c->addAbilityParam(name, value);
        }
    }
}

void MainWindow::on_toolButtonExportChoosePath_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, "Select file", lastExportPath, "Properties (*.properties);;All files (*.*)", 0, 0);
    if( path.isEmpty() )
        return;
    setExportPath(path);
}

void MainWindow::on_toolButtonImportChoosePath_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, "Select file", lastImportPath, "Properties (*.properties);;All files (*.*)", 0, 0);
    if( path.isEmpty() )
        return;

    //QThread::msleep(300);
    emit signalLoadProperties(path);
}

void MainWindow::on_toolButtonParamsRemove_clicked()
{
    QTableWidget *table = ui->tableWidgetParams;
    int r = table->currentRow();
    if (r < 0) return;
    //
    int rmin = r;
    QList<QTableWidgetItem *> items = table->selectedItems();
    foreach (QTableWidgetItem *i, items) {
        int row = table->row(i);
        QString name = Utils::cellText(table, row, 0);
        if (name != "") {
            activeConfig->removeFromParams(name);
        }
        table->removeRow(row);
        if (row < rmin) {
            rmin = row;
        }
    }
    int rows = table->rowCount();
    if (rmin >= rows) {
        rmin = rows-1;
    }
    table->selectRow(rmin);
}

void MainWindow::on_toolButtonParamsAdd_clicked()
{
    isListenToTableChanges = false;
    QTableWidget *table = ui->tableWidgetParams;
    int count = table->rowCount();
    table->setRowCount(count+1);
    table->setCurrentCell(count, 0);
    isListenToTableChanges = true;
}

void MainWindow::on_tableWidgetManifest_cellClicked(int row, int /*column*/)
{
    manifestStoredName = Utils::cellText(ui->tableWidgetManifest,row,0);
}

void MainWindow::on_tableWidgetManifest_cellChanged(int row, int column)
{
    if (!isListenToTableChanges) return;
    QTableWidget *table = ui->tableWidgetManifest;
    QString name = Utils::cellText(table, row, 0);
    if (name != "") {
        if (column == 0 && activeConfig->hasManifestParam(name)) {
            QMessageBox::information(this, "Manifest Params", "Param name '"+name+"' already exists!");
            isListenToTableChanges = false;
            table->item(row, 0)->setText(manifestStoredName);
            isListenToTableChanges = true;
            return;
        }
        QString value = Utils::cellText(table, row, 1);
        activeConfig->changeManifestParam(manifestStoredName, name, value);
        manifestStoredName = name;
    }
}

void MainWindow::on_tableWidgetAbilities_cellClicked(int row, int /*column*/)
{
    abilityStoredName = Utils::cellText(ui->tableWidgetAbilities,row,0);
}

void MainWindow::on_tableWidgetAbilities_cellChanged(int row, int column)
{
    if (!isListenToTableChanges) return;
    QTableWidget *table = ui->tableWidgetAbilities;
    QString name = Utils::cellText(table, row, 0);
    if (name != "") {
        if (column == 0 && activeConfig->hasAbilityParam(name)) {
            QMessageBox::information(this, "Ability Params", "Param name '"+name+"' already exists!");
            isListenToTableChanges = false;
            table->item(row, 0)->setText(abilityStoredName);
            isListenToTableChanges = true;
            return;
        }
        QString value = Utils::cellText(table, row, 1);
        activeConfig->changeAbilityParam(abilityStoredName, name, value);
        abilityStoredName = name;
    }
}

void MainWindow::on_tableWidgetParams_cellClicked(int row, int /*column*/)
{
    paramsStoredName = Utils::cellText(ui->tableWidgetParams,row,0);
}

void MainWindow::on_tableWidgetParams_cellChanged(int row, int column)
{
    if (!isListenToTableChanges) return;
    QTableWidget *table = ui->tableWidgetParams;
    QString name = Utils::cellText(table, row, 0);
    if (name != "") {
        if (column == 0 && activeConfig->hasParamsParam(name)) {
            QMessageBox::information(this, "All Params", "Param name '"+name+"' already exists!");
            bool old = isListenToTableChanges;
            isListenToTableChanges = false;
            table->item(row, 0)->setText(paramsStoredName);
            isListenToTableChanges = old;
            return;
        }
        QString value = Utils::cellText(table, row, 1);
        activeConfig->changeParamsParam(paramsStoredName, name, value);
        paramsStoredName = name;
    }
}

void MainWindow::on_toolButtonConfigAdd_clicked()
{
    QString name = "";
    while (true) {
        bool ok = false;
        name = QInputDialog::getText(this, "New Config", "Enter config name:", QLineEdit::Normal, name, &ok);
        if (!ok) return;
        if (name == "") {
            QMessageBox::information(this, "New Config", "Name is empty!");
        } else if (manager.has(name)) {
            QMessageBox::information(this, "New Config", "Name '"+name+"' already exists!");
        } else {
            break;
        }
    }
    manager.add(name);
    ui->listWidgetConfigs->addItem(name);
}

void MainWindow::on_toolButtonConfigRename_clicked()
{
    if (activeConfig == 0) return;
    QString confName = activeConfig->name;
    QString name = confName;
    while (true) {
        bool ok = false;
        name = QInputDialog::getText(this, "Rename Config", "Enter new name for "+confName+":", QLineEdit::Normal, name, &ok);
        if (!ok) return;
        if (name == "") {
            QMessageBox::information(this, "Rename Config", "Name is empty!");
        } else if (manager.has(name)) {
            QMessageBox::information(this, "Rename Config", "Name '"+name+"' already exists!");
        } else {
            break;
        }
    }
    manager.rename(confName, name);
    QListWidgetItem *i = ui->listWidgetConfigs->item(ui->listWidgetConfigs->currentRow());
    i->setText(name);
    ui->labelActiveConfig->setText("active: "+name);
    if (activeConfig == pinnedConfig) {
        ui->labelPinnedConfig->setText(name);
    }
}

void MainWindow::on_toolButtonConfigClone_clicked()
{
    if (activeConfig == 0) return;
    QString confName = activeConfig->name;
    QString name = confName;
    while (true) {
        bool ok = false;
        name = QInputDialog::getText(this, "Clone Config", "Enter new name for "+confName+":", QLineEdit::Normal, name, &ok);
        if (!ok) return;
        if (name == "") {
            QMessageBox::information(this, "Clone Config", "Name is empty!");
        } else if (manager.has(name)) {
            QMessageBox::information(this, "Clone Config", "Name '"+name+"' already exists!");
        } else {
            break;
        }
    }
    manager.clone(confName, name);
    ui->listWidgetConfigs->addItem(name);
}

void MainWindow::on_pushButtonExport_clicked()
{
    statusBar()->showMessage("Exporting...");
    QString path = ui->labelExportPath->text();
    QFile f(path);
    // save backup
    if (f.exists()) {
        QFile f2(path+"_");
        f2.remove(); // need to remove for success renaming
        QFile f3(path);
        f3.rename(path+"_");
    }
    // save new data
    bool ok = f.open(QFile::WriteOnly);
    if (!ok) {
        QMessageBox::information(this, "Export", "Error while saving data to "+path);
        return;
    }
    f.write(manager.exportAll().toUtf8());
    f.close();
    statusBar()->showMessage("Exporting... done.");
}

void MainWindow::on_toolButtonChooseFilteringRootPath_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Select the 'src' folder", lastFilteringRootPath);
    if( path.isEmpty() )
        return;
    setFilteringRootPath(path);
}

void MainWindow::setItemState(QTreeWidgetItem *item, Qt::CheckState state, bool updateParentItemState, bool updateChildrenItemsState)
{
    bool hasChildren = (item->childCount()>0);
    /*Qt::CheckState st = state;
    if (!hasChildren && st == Qt::PartiallyChecked) {
        st = Qt::Checked;
    }*/
    item->setCheckState(0, state);
    if (activeConfig != 0) {
        activeConfig->filter->set(itemPath(item), item->checkState(0), hasChildren);
    }
    if (updateParentItemState) {
        QTreeWidgetItem *parent = item->parent();
        if (parent) {
            int cnt = parent->childCount();
            int unchecked = 0, checked = 0, partially = 0;
            for (int k = 0; k < cnt; ++k) {
                QTreeWidgetItem *i = parent->child(k);
                if (i->checkState(0) == Qt::Checked) {
                    ++checked;
                } else if (i->checkState(0) == Qt::Unchecked) {
                    ++unchecked;
                } else {
                    ++partially;
                }
            }
            Qt::CheckState newParentState;
            if (unchecked == cnt) {
                newParentState = Qt::Unchecked;
            } else if (checked == cnt) {
                newParentState = Qt::Checked;
            } else {
                newParentState = Qt::PartiallyChecked;
            }
            Qt::CheckState parentState = parent->checkState(0);
            if (newParentState != parentState) {
                /*if (activeConfig != 0) {
                    activeConfig->filter.set(itemPath(parent), newParentState, true);
                }
                setItemData(parent, newParentState);*/
                setItemState(parent, newParentState, updateParentItemState);
            }
        }
    }
    // set for checked & unchecked, but not partially
    if (updateChildrenItemsState && hasChildren) {
        int cnt = item->childCount();
        for (int k = 0; k < cnt; ++k) {
            QTreeWidgetItem *i = item->child(k);
            setItemState(i, state, false, true);
        }
    }
}

void MainWindow::addItemsToTree(QTreeWidget *tree, QTreeWidgetItem *parentItem, const QFileInfoList &list)
{
    foreach (QFileInfo info, list) {
        QString name = info.fileName();
        if (name == "." || name == "..") continue;
        QTreeWidgetItem *item;
        if (parentItem) {
            item = new QTreeWidgetItem(parentItem);
        } else {
            item = new QTreeWidgetItem(tree);
        }
        item->setText(0, name);
        if (info.isDir()) {
            QDir dir(info.absoluteFilePath());
            QFileInfoList list2 = dir.entryInfoList();
            addItemsToTree(tree, item, list2);
        }
    }
}

void MainWindow::scanItemsInTree(QTreeWidgetItem *parentItem, int type)
{
    int cnt = parentItem->childCount();
    for (int k = 0; k < cnt; ++k) {
        QTreeWidgetItem *item = parentItem->child(k);
        onScanItemsInTreeCallback(item, type);
        if (item->childCount() > 0) {
            scanItemsInTree(item, type);
        }
    }
}

void MainWindow::onScanItemsInTreeCallback(QTreeWidgetItem *item, int type)
{
    if (activeConfig == 0) return;
    QString path = itemPath(item);
    if (type == 0) {
        // reset to default
        /*Qt::CheckState state = Qt::Checked;
        item->setCheckState(0, state);
        if (item->childCount() > 0) {
            setItemData(item, state);
        }*/
        /*if (activeConfig != 0) {
            activeConfig->filter.set(itemPath(item), state, item->childCount() > 0);
        }*/
    } else if (type == 1) {
        // set state from filter
        FilterItem *fi = activeConfig->filter->find(path);
        if (fi != 0) {
            Qt::CheckState state = fi->state;

            bool setForChildren = (item->childCount() > 0 && (state == Qt::Checked || state == Qt::Unchecked));

            setItemState(item, state, true, setForChildren);
        } else {
            setItemState(item, Qt::Checked);
        }
    }
}

QString MainWindow::itemPath(QTreeWidgetItem *item) {
    QString s = "";
    while (item != 0) {
        s = "/"+item->text(0) + s;
        item = item->parent();
    }
    return "res"+s;
}

void MainWindow::setFilteringRootPath(QString path)
{
    if (path.isEmpty() || path == lastFilteringRootPath) return;
    lastFilteringRootPath = path;
    settings()->setValue(FILTERING_PATH, lastFilteringRootPath);
    ui->labelFilteringRootPath->setText(lastFilteringRootPath);
    //
    ui->treeSourceFiltering->clear();
    QDir dir(path);
    QFileInfoList list = dir.entryInfoList();
    addItemsToTree(ui->treeSourceFiltering, 0, list);

    adjustConfigFilter();

}

void MainWindow::adjustConfigFilter()
{
    if (activeConfig == 0) return;
    if (activeConfig->filter->isEmpty()) return;
    scanItemsInTree(ui->treeSourceFiltering->invisibleRootItem(), 0);
    scanItemsInTree(ui->treeSourceFiltering->invisibleRootItem(), 1);
}

void MainWindow::on_treeSourceFiltering_itemClicked(QTreeWidgetItem *item, int column)
{
    if (activeConfig == 0) return;
    FilterItem *fi = activeConfig->filter->find(itemPath(item));
    if (fi->isFolder) {// restore folder state
        item->setCheckState(column, fi->state);
    } else {
        setItemState(item, item->checkState(column));
    }
}

void MainWindow::on_toolButtonFilteringCloneFromPinned_clicked()
{
    if (pinnedConfig == 0 || activeConfig == 0) return;
    if (activeConfig != pinnedConfig) {
        activeConfig->cloneFilter(pinnedConfig);
        adjustConfigFilter();
    }
}

void MainWindow::on_treeSourceFiltering_customContextMenuRequested(const QPoint &pos)
{
    QTreeWidget *tree = ui->treeSourceFiltering;
    QTreeWidgetItem *i = tree->itemAt(pos);
    if (i != 0) {
        QAction *act = filterPopup->exec(tree->mapToGlobal(pos));
        if (act != 0) {
            Qt::CheckState state = (act->text() == "Uncheck") ? Qt::Unchecked : Qt::Checked;
            setItemState(i, state, true, true);
        }
    }
}

void MainWindow::on_toolButtonParamsCloneFromPinned_clicked()
{
    if (activeConfig == 0 || pinnedConfig == 0) return;
    if (activeConfig == pinnedConfig) return;
    activeConfig->cloneParams(pinnedConfig);
    fillParamsUi();
}

void MainWindow::on_toolButtonReloadConfig_clicked()
{
    QString path = ui->labelImportPath->text();
    if (!path.isEmpty()) {
        loadProperties(path);
    }
}

void MainWindow::on_tableWidgetManifest_customContextMenuRequested(const QPoint &pos)
{
    QTableWidget *table = ui->tableWidgetManifest;
    QTableWidgetItem *i = table->itemAt(pos);

    if (i == 0)
        return;

    QAction *act = manifestPopup->exec(table->mapToGlobal(pos));
    if (act != 0) {
        int ok = 0;
        QString paramName = table->item(table->currentRow(), 0)->text();
        QString paramValue = table->item(table->currentRow(), 1)->text();
        foreach (QListWidgetItem *item, ui->listWidgetConfigs->selectedItems()){
            Config *c = manager.find(item->text());
            if (c != 0){
                c->changeManifestParam(paramName, paramName, paramValue);
                ++ok;
            }
        }
        statusBar()->showMessage(QString("Manifest value applied to %1 configs.").arg(ok));
    }

}
